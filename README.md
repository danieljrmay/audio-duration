# audio-duration

This library has been deprecated in favour of
[sampled_data_duration](https://crates.io/crates/sampled_data_duration).

Please contact me if you would like to take ownership of the
`audio-duration` crate name on `crates.io`.

# Audio Duration TODOs #

* [ ] Write documentation.
* [ ] Refactor to use "chrono" or "timecode" crate?
* [ ] Merge into "chrono" or other "timecode" crate?
* [ ] Consider renaming sample-data-duration or similar. This would
      have the following structs: SampledDataDuration,
      ConstantRateSampledDataDuration{number_of_samples,
      sampling_rate}, VariableRateSampledDataDuration{}
